#!/usr/bin/env bash

# colors
bold=$(tput bold 2> /dev/null || echo -e "\x1b[1m")
red=$(tput setaf 1 2> /dev/null || echo -e "\x1b[38;5;1m")
green=$(tput setaf 2 2> /dev/null || echo -e "\x1b[38;5;2m")
yellow=$(tput setaf 3 2> /dev/null || echo -e "\x1b[38;5;3m")
blue=$(tput setaf 4 2> /dev/null || echo -e "\x1b[38;5;4m")
cyan=$(tput setaf 6 2> /dev/null || echo -e "\x1b[38;5;6m")
reset=$(tput sgr0 2> /dev/null || echo -e "\x1b[0m")

# misc. config
image_name="gptournament"
if [[ "${CI_COMMIT_BRANCH}" = "master" ]]; then
	image_tag="latest"
else
	image_tag="${CI_COMMIT_REF_SLUG}"
fi

# Use vfs with buildah. Docker offers overlayfs as a default, but buildah
# cannot stack overlayfs on top of another overlayfs filesystem.
export STORAGE_DRIVER=vfs

set -ex

echo "${green}Building container${reset}"
container=$(buildah from --pull docker.io/debian:buster)

# install required things
buildah run ${container} -- apt-get update
buildah run ${container} -- apt-get install -y libgomp1 libssl1.1 ca-certificates curl unzip sqlite3
buildah run ${container} -- rm -rf /var/lib/apt/lists

# get libtorch
buildah run ${container} -- mkdir /app
buildah run ${container} -- curl -Lo /app/libtorch.zip https://download.pytorch.org/libtorch/cpu/libtorch-cxx11-abi-shared-with-deps-1.6.0%2Bcpu.zip
buildah run ${container} -- unzip /app/libtorch.zip "libtorch/lib/*" -d /app
buildah run ${container} -- rm /app/libtorch.zip

# copy files
buildah copy ${container} gptournament /app
buildah copy ${container} docker/run.sh /app

# configuration
buildah config --entrypoint '["/app/run.sh"]' --cmd '' --port 8000 --workingdir "/app" ${container}

echo "${green}Committing container${reset}"
buildah commit --format docker ${container} ${image_name}

if [[ -n "${CI_REGISTRY}" ]]; then
	echo -e "${green}Pushing image${reset}"
	buildah push ${image_name} "${CI_REGISTRY_IMAGE}:${image_tag}"
fi
