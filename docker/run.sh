#!/usr/bin/env bash

export LD_LIBRARY_PATH="/app/libtorch/lib:${LD_LIBRARY_PATH}"

exec ./gptournament "$@"
