use gpt::model::{
    processing::{TokenDecoder, TokenProcessorBuilder},
    GenerationOptions, GeneratorModel, ThreadedModel,
};

use std::io::{self, Write};

fn main() {
    gpt::set_model_path_to_cwd();

    let text = "Hello world!";
    let model = {
        let mut model = ThreadedModel::new();
        model.wait_for_load().unwrap();
        model
    };

    let options = GenerationOptions::default();

    let generator = model.create_token_generator(text, options).unwrap();
    let processing = TokenProcessorBuilder::new()
        .with_full_sentences()
        .with_min_length(50)
        .with_max_length(200)
        .build(&model, generator);
    let decoded = TokenDecoder::new(processing, &model);
    print!("{}", text);
    for token in decoded {
        print!("{}", token);
        let _ = io::stdout().flush();
    }
}
