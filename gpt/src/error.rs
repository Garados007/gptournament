use crate::model::threaded::ModelResult;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ThreadedModelError {
    #[error("Unexpected message from model thread: {0:?}")]
    UnexpectedModelResult(ModelResult),
    #[error("Error loading model")]
    ModelLoadError(#[from] GptError),
    #[error("Model not loaded")]
    ModelNotLoaded,
    #[error("Couldn't receive message")]
    MessageReceiveError,
    #[error("Couldn't send message")]
    MessageSendError,
}

impl From<std::sync::mpsc::RecvError> for ThreadedModelError {
    fn from(_: std::sync::mpsc::RecvError) -> Self {
        Self::MessageReceiveError
    }
}

impl<T> From<std::sync::mpsc::SendError<T>> for ThreadedModelError {
    fn from(_: std::sync::mpsc::SendError<T>) -> Self {
        Self::MessageSendError
    }
}

#[derive(Error, Debug)]
pub enum GptError {
    #[error("Failed to download model file \"{resource}\"")]
    FileDownloadError {
        resource: String,
        #[source]
        source: rust_bert::RustBertError,
    },
    #[error("Failed to create tokenizer")]
    TokenizerCreationError(#[from] rust_tokenizers::preprocessing::error::TokenizerError),
    #[error("Failed to load model")]
    ModelLoadError(#[from] tch::TchError),
    #[error("Failed to create pipeline")]
    PipelineError(#[source] rust_bert::RustBertError),
}

impl GptError {
    pub fn file_download_error(resource: &str, source: rust_bert::RustBertError) -> Self {
        Self::FileDownloadError {
            resource: String::from(resource),
            source,
        }
    }

    pub fn pipeline_error(source: rust_bert::RustBertError) -> Self {
        Self::PipelineError(source)
    }
}

#[derive(Error, Debug)]
pub enum ModelError {
    #[error("Error from threaded model: {0}")]
    ThreadedModelError(#[from] ThreadedModelError),
    #[error("Unspecified error: {0}")]
    Error(Box<dyn std::error::Error>),
}

impl ModelError {
    fn from_error<E: std::error::Error + 'static>(e: E) -> Self {
        Self::Error(Box::new(e))
    }
}

#[derive(Error, Debug)]
pub enum GameError {
    #[error("A player with the name {0} already exists")]
    PlayerExists(String),
    #[error("A player with the name {0} does not exist")]
    UnknownPlayer(String),
}
