use crate::error::{GameError, ThreadedModelError};
use crate::model::{
    processing::{BaseTokenDecoder, TokenProcessor, TokenProcessorBuilder},
    GenerationOptions, ThreadedModel, ThreadedTokenGenerator, TokenGenerator,
};
use std::collections::HashMap;
use std::ops::Deref;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum PlayerState {
    Alive,
    Dead,
}

#[derive(Clone, Debug)]
pub struct PlayerData {
    state: PlayerState,
}

pub struct Player<'a> {
    name: String,
    data: &'a mut PlayerData,
    players_alive: &'a mut usize,
}

pub struct Game<'a> {
    text: String,
    model: &'a ThreadedModel,
    generation_options: GenerationOptions,
    generator: TokenProcessor<ThreadedTokenGenerator>,
    decoder: BaseTokenDecoder<'a, ThreadedModel>,
    players: HashMap<String, PlayerData>,
    player_order: Vec<String>,
    current_player: usize,
    players_alive: usize,
}

impl<'a> Game<'a> {
    pub fn new(
        model: &'a ThreadedModel,
        generation_options: GenerationOptions,
    ) -> Result<Self, ThreadedModelError> {
        let generator = Self::build_generator(model, String::from(""), generation_options)?;
        let decoder = BaseTokenDecoder::new(model);
        Ok(Self {
            text: String::from(""),
            model,
            generation_options,
            generator,
            decoder,
            players: HashMap::new(),
            player_order: Vec::new(),
            current_player: 0,
            players_alive: 0,
        })
    }

    pub fn model(&self) -> &'a ThreadedModel {
        self.model
    }

    pub fn add_player(&mut self, name: String) -> Result<(), GameError> {
        if self.players.contains_key(&name) {
            return Err(GameError::PlayerExists(name));
        }
        self.player_order.push(name.clone());
        self.players.insert(
            name,
            PlayerData {
                state: PlayerState::Alive,
            },
        );
        self.players_alive += 1;
        Ok(())
    }

    pub fn player_by_name(&mut self, name: String) -> Result<Player, GameError> {
        if let Some(data) = self.players.get_mut(&name) {
            Ok(Player {
                name,
                data,
                players_alive: &mut self.players_alive,
            })
        } else {
            Err(GameError::UnknownPlayer(name))
        }
    }

    pub fn current_player_name(&self) -> &str {
        self.player_order.get(self.current_player).unwrap()
    }

    pub fn current_player(&mut self) -> Player {
        let name = String::from(self.current_player_name());
        self.player_by_name(name).unwrap()
    }

    pub fn next_player(&mut self) -> Player {
        if self.players_alive <= 1 {
            return self.current_player();
        }
        self.current_player = (self.current_player + 1) % self.players.len();
        if self.current_player().is_dead() {
            self.next_player()
        } else {
            self.current_player()
        }
    }

    pub fn winner(&mut self) -> Option<Player> {
        if self.players_alive != 1 {
            return None;
        }
        let mut alive_players = self
            .players
            .iter_mut()
            .filter(|p| p.1.is_alive())
            .collect::<Vec<_>>();
        if alive_players.len() != 1 {
            None
        } else {
            let (name, data) = alive_players.pop().unwrap();
            Some(Player {
                name: name.clone(),
                data,
                players_alive: &mut self.players_alive,
            })
        }
    }

    pub fn add_input_line(&mut self, new_line: &str) -> Result<(), ThreadedModelError> {
        self.text.push_str(new_line);
        self.generator =
            Self::build_generator(self.model, self.text.clone(), self.generation_options)?;
        Ok(())
    }

    pub fn next_token(&mut self) -> Option<String> {
        self.decoder.next_token(&mut self.generator)
    }

    pub fn reset_generator(&mut self) {
        self.generator.reset()
    }

    fn build_generator(
        model: &ThreadedModel,
        text: String,
        generation_options: GenerationOptions,
    ) -> Result<TokenProcessor<ThreadedTokenGenerator>, ThreadedModelError> {
        let generator = model.get_generator(text, generation_options)?;
        let processor = TokenProcessorBuilder::new()
            .with_full_sentences()
            .with_min_length(50)
            .with_max_length(200)
            .build(model, generator);
        Ok(processor)
    }
}

impl<'a> Iterator for Game<'a> {
    type Item = String;

    fn next(&mut self) -> Option<String> {
        self.next_token()
    }
}

impl PlayerData {
    pub fn state(&self) -> PlayerState {
        self.state
    }

    pub fn is_alive(&self) -> bool {
        self.state == PlayerState::Alive
    }

    pub fn is_dead(&self) -> bool {
        self.state == PlayerState::Dead
    }
}

impl Deref for Player<'_> {
    type Target = PlayerData;

    fn deref(&self) -> &Self::Target {
        self.data
    }
}

impl Player<'_> {
    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn state(&self) -> PlayerState {
        self.data.state()
    }

    pub fn is_alive(&self) -> bool {
        self.data.is_alive()
    }

    pub fn is_dead(&self) -> bool {
        self.data.is_dead()
    }

    pub fn kill(&mut self) {
        if self.is_alive() {
            self.data.state = PlayerState::Dead;
            *self.players_alive -= 1;
        }
    }

    pub fn revive(&mut self) {
        if self.is_dead() {
            self.data.state = PlayerState::Alive;
            *self.players_alive += 1;
        }
    }
}
