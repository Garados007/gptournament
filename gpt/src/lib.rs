#![feature(crate_visibility_modifier, generic_associated_types)]

mod error;
mod filter;
pub mod game;
pub mod model;

pub use error::{GptError, ModelError, ThreadedModelError};
pub use filter::Filter;

use std::env;
use std::ffi::OsStr;

pub fn set_model_path_to_cwd() {
    env::set_var("RUSTBERT_CACHE", env::current_dir().unwrap().join("models"));
}

pub fn set_model_path<V: AsRef<OsStr>>(path: V) {
    env::set_var("RUSTBERT_CACHE", path);
}
