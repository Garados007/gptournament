use gpt::{
    game::Game,
    model::{GenerationOptions, ThreadedModel},
};

use std::io::{self, Write};
use std::thread;

enum Action {
    More,
    Next,
    Kill,
    Revive,
}

fn main() {
    gpt::set_model_path_to_cwd();

    let model_thread = thread::spawn(|| {
        let mut model = ThreadedModel::new();
        model.wait_for_load().unwrap();
        model
    });

    let mut players = Vec::new();
    let stdin = io::stdin();
    println!("enter player names:");
    loop {
        let mut name = String::new();
        match stdin.read_line(&mut name) {
            Ok(0) | Ok(1) | Err(_) => break,
            Ok(_) => {
                let name = name.trim();
                players.push(String::from(name));
            }
        }
    }

    println!("waiting for model to load...");
    let model = model_thread.join().expect("model");
    let options = GenerationOptions::default();
    let mut game = Game::new(&model, options).expect("game");
    for player in players.into_iter() {
        game.add_player(player).expect("player");
    }

    loop {
        println!("Current player: {}", game.current_player().name());
        let mut line = String::new();
        while let Err(_) | Ok(0) = stdin.read_line(&mut line) {}
        game.add_input_line(&line).expect("line");
        loop {
            for token in &mut game {
                print!("{}", token);
                let _ = io::stdout().flush();
            }
            println!();
            let result = || loop {
                print!("[M]ore text/[N]ext player/[K]ill player/[R]evive Player ");
                let _ = io::stdout().flush();
                let mut input = String::new();
                match stdin.read_line(&mut input) {
                    Err(_) | Ok(0) | Ok(1) => continue,
                    Ok(_) => {
                        return match input.trim() {
                            "M" | "m" => Action::More,
                            "N" | "n" => Action::Next,
                            "K" | "k" => Action::Kill,
                            "R" | "r" => Action::Revive,
                            _ => continue,
                        }
                    }
                }
            };
            let result = result();
            match result {
                Action::Kill => {
                    game.current_player().kill();
                    break;
                }
                Action::Revive => {
                    game.current_player().revive();
                    break;
                }
                Action::Next => break,
                Action::More => {
                    game.reset_generator();
                    continue;
                }
            }
        }
        game.next_player();
        if let Some(winner) = game.winner() {
            println!("{} has won the game!", winner.name());
            break;
        }
    }
}
