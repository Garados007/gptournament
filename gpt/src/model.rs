mod base_model;
pub mod processing;
crate mod threaded;

use crate::ModelError;
pub use base_model::{GptTokenGenerator, Model};
pub use threaded::{ThreadedModel, ThreadedTokenGenerator};

#[derive(Debug, Copy, Clone)]
pub struct GenerationOptions {
    pub top_k: i64,
    pub top_p: f64,
    pub temperature: f64,
}

impl Default for GenerationOptions {
    fn default() -> Self {
        GenerationOptions {
            top_k: 40,
            top_p: 0.9,
            temperature: 1.0,
        }
    }
}

pub trait TokenGenerator: Iterator<Item = i64> {
    fn reset(&mut self);
}

pub trait ModelTokens {
    fn decode_text(&self, tokens: Vec<i64>) -> String;
    fn get_token_id(&self, token: &str) -> i64;
}

pub trait GeneratorModel: ModelTokens {
    type Generator<'a>: TokenGenerator;

    fn create_token_generator(
        &self,
        text: &str,
        generation_options: GenerationOptions,
    ) -> Result<Self::Generator<'_>, ModelError>;
}
