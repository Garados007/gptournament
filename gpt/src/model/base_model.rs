use super::{GenerationOptions, GeneratorModel, ModelTokens, TokenGenerator};
use crate::error::{GptError, ModelError};
use rust_bert::gpt2::{
    GPT2LMHeadModel, Gpt2Config, Gpt2ConfigResources, Gpt2MergesResources, Gpt2ModelResources,
    Gpt2VocabResources,
};
use rust_bert::pipelines::generation::{Cache, LMHeadModel, LMModelOutput};
use rust_bert::resources::{RemoteResource, Resource};
use rust_bert::Config;
use rust_tokenizers::{Gpt2Tokenizer, Gpt2Vocab, Tokenizer, TruncationStrategy, Vocab};
use std::cmp::{max, min};
use std::iter::Iterator;
use tch::kind::Kind::{Bool, Float, Int64};
use tch::{nn, no_grad, Device, Tensor};

pub struct Model {
    device: Device,
    model: GPT2LMHeadModel,
    tokenizer: Gpt2Tokenizer,
    eos_token_id: i64,
}

impl Model {
    pub fn new() -> Result<Self, GptError> {
        log::debug!("Creating new GPT model");
        let config_res = Resource::Remote(RemoteResource::from_pretrained(
            Gpt2ConfigResources::GPT2_LARGE,
        ));
        let vocab_res = Resource::Remote(RemoteResource::from_pretrained(
            Gpt2VocabResources::GPT2_LARGE,
        ));
        let merges_res = Resource::Remote(RemoteResource::from_pretrained(
            Gpt2MergesResources::GPT2_LARGE,
        ));
        let model_res = Resource::Remote(RemoteResource::from_pretrained(
            Gpt2ModelResources::GPT2_LARGE,
        ));
        let config_path = config_res
            .get_local_path()
            .map_err(|e| GptError::file_download_error("GPT2 Large config", e))?;
        let vocab_path = vocab_res
            .get_local_path()
            .map_err(|e| GptError::file_download_error("GPT2 Large vocab", e))?;
        let merges_path = merges_res
            .get_local_path()
            .map_err(|e| GptError::file_download_error("GPT2 Large merges", e))?;
        let model_path = model_res
            .get_local_path()
            .map_err(|e| GptError::file_download_error("GPT2 Large model", e))?;

        let device = Device::cuda_if_available();
        log::debug!("Model running on device {:?}", device);
        let mut vs = nn::VarStore::new(device);
        let tokenizer = Gpt2Tokenizer::from_file(
            vocab_path.to_str().unwrap(),
            merges_path.to_str().unwrap(),
            false,
        )?;
        let config = Gpt2Config::from_file(config_path);
        let model = GPT2LMHeadModel::new(&vs.root(), &config);
        vs.load(model_path)?;

        let eos_token_id = tokenizer.vocab().token_to_id(Gpt2Vocab::eos_value());

        Ok(Model {
            device,
            tokenizer,
            model,
            eos_token_id,
        })
    }

    pub fn encode_text(&self, text: &str) -> Tensor {
        let tokens =
            self.tokenizer
                .encode_list(vec![text], 128, &TruncationStrategy::LongestFirst, 0);

        let max_len = tokens
            .iter()
            .map(|input| input.token_ids.len())
            .max()
            .unwrap();
        let tokens = tokens
            .iter()
            .map(|input| input.token_ids.clone())
            .map(|mut input| {
                input.extend(vec![0; max_len - input.len()]);
                input
            })
            .map(|input| Tensor::of_slice(&input))
            .collect::<Vec<_>>();
        Tensor::stack(&tokens, 0).to(self.device)
    }

    pub fn decode_text(&self, tokens: Vec<i64>) -> String {
        self.tokenizer.decode(tokens, true, true)
    }

    pub fn get_generator(&self, inputs: Tensor, options: GenerationOptions) -> GptTokenGenerator {
        GptTokenGenerator {
            model: &self,
            inputs: inputs.copy(),
            attention_mask: inputs.ones_like().to_kind(Int64),
            past: Some(Cache::None),
            options,
        }
    }

    pub fn generate_text(
        &self,
        inputs: Tensor,
        options: GenerationOptions,
        max_len: i64,
    ) -> Vec<i64> {
        let generator = self.get_generator(inputs, options);
        (0..max_len).zip(generator).map(|item| item.1).collect()
    }
}

impl ModelTokens for Model {
    fn decode_text(&self, tokens: Vec<i64>) -> String {
        self.decode_text(tokens)
    }

    fn get_token_id(&self, token: &str) -> i64 {
        self.tokenizer.vocab().token_to_id(token)
    }
}

impl GeneratorModel for Model {
    type Generator<'a> = GptTokenGenerator<'a>;

    fn create_token_generator(
        &self,
        text: &str,
        generation_options: GenerationOptions,
    ) -> Result<Self::Generator<'_>, ModelError> {
        let inputs = self.encode_text(text);
        Ok(self.get_generator(inputs, generation_options))
    }
}

pub struct GptTokenGenerator<'a> {
    model: &'a Model,
    inputs: Tensor,
    attention_mask: Tensor,
    past: Option<Cache>,
    options: GenerationOptions,
}

impl<'a> GptTokenGenerator<'a> {
    fn generate(&mut self) -> Option<i64> {
        no_grad(|| {
            log::trace!("Generating new token");
            let (prep_input, prep_past) = match self.past.take() {
                Some(Cache::GPT2Cache(Some(past))) => (
                    self.inputs.select(1, -1).unsqueeze(-1),
                    Cache::GPT2Cache(Some(past)),
                ),
                _ => (self.inputs.copy(), Cache::None),
            };
            let prep_input = Some(prep_input);

            let lm_output: LMModelOutput;
            let attention_mask = Some(self.attention_mask.copy());
            loop {
                let cloned_past = match &prep_past {
                    Cache::GPT2Cache(Some(past)) => {
                        Cache::GPT2Cache(Some(past.iter().map(Tensor::copy).collect()))
                    }
                    _ => Cache::None,
                };
                let model = &self.model.model;
                let result = std::panic::catch_unwind(|| {
                    model
                        .forward_t(
                            &prep_input,
                            cloned_past,
                            &attention_mask,
                            &None,
                            &None,
                            &None,
                            None,
                            &None,
                            false,
                        )
                        .unwrap()
                });
                match result {
                    Ok(output) => {
                        lm_output = output;
                        break;
                    }
                    Err(e) => {
                        log::warn!(
                            "Caught error in model forward pass, will retry. Error: {:?}\n",
                            e
                        );
                    }
                }
            }
            let lm_output = lm_output;
            let outputs = lm_output.lm_logits;
            self.past = Some(lm_output.cache);

            let mut next_token_logits = outputs.select(1, -1);
            next_token_logits /= self.options.temperature;
            top_k_top_p_logits(
                &mut next_token_logits,
                self.options.top_k,
                self.options.top_p,
                1,
            );
            let probabilities = next_token_logits.softmax(-1, Float);
            let next_token = probabilities.multinomial(1, false).squeeze1(1);

            let next_token_id = next_token.int64_value(&[0]);
            if next_token_id == self.model.eos_token_id {
                None
            } else {
                self.inputs = Tensor::cat(
                    &[self.inputs.as_ref(), next_token.unsqueeze(-1).as_ref()],
                    -1,
                );
                self.attention_mask = Tensor::cat(
                    &[
                        self.attention_mask.as_ref(),
                        Tensor::ones(
                            &[*self.attention_mask.size().first().unwrap(), 1],
                            (Int64, self.attention_mask.device()),
                        )
                        .as_ref(),
                    ],
                    -1,
                );
                Some(next_token_id)
            }
        })
    }
}

impl Iterator for GptTokenGenerator<'_> {
    type Item = i64;

    fn next(&mut self) -> Option<Self::Item> {
        self.generate()
    }
}

impl TokenGenerator for GptTokenGenerator<'_> {
    fn reset(&mut self) {}
}

fn top_k_top_p_logits(logits: &mut Tensor, top_k: i64, top_p: f64, min_tokens_to_keep: i64) {
    let vocab_size = *logits.size().last().unwrap();

    if top_k > 0 {
        let top_k = vocab_size - min(max(top_k, min_tokens_to_keep), vocab_size);

        let (_, indices_to_remove) = logits.topk(top_k, -1, false, false);
        for index in 0..*logits.size().first().unwrap() {
            let _ = logits.get(index).index_fill_(
                0,
                &indices_to_remove.get(index),
                std::f64::NEG_INFINITY,
            );
        }
    }
    if top_p < 1. {
        let (sorted_logits, sorted_indices) = logits.sort(-1, true);
        let cumulative_probabilites = sorted_logits.softmax(-1, Float).cumsum(-1, Float);
        let mut sorted_indices_to_remove = cumulative_probabilites.ge(top_p).to_kind(Int64);
        if min_tokens_to_keep > 1 {
            let _ = sorted_indices_to_remove.index_fill_(
                1,
                &Tensor::arange1(1, vocab_size, (Int64, logits.device())),
                0,
            );
        }
        let _ = sorted_indices_to_remove.index_copy_(
            1,
            &Tensor::arange1(1, vocab_size, (Int64, logits.device())),
            &sorted_indices_to_remove
                .slice(1, 0, vocab_size - 1, 1)
                .copy(),
        );
        let _ = sorted_indices_to_remove.index_fill_(
            1,
            &Tensor::of_slice(&[0])
                .to_kind(Int64)
                .to_device(sorted_indices_to_remove.device()),
            0,
        );
        let indices_to_remove = sorted_indices_to_remove
            .scatter(1, &sorted_indices, &sorted_indices_to_remove)
            .to_kind(Bool);
        let _ = logits.masked_fill_(&indices_to_remove, std::f64::NEG_INFINITY);
    }
}
