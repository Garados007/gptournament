use super::{GeneratorModel, ModelTokens, TokenGenerator};

pub struct TokenProcessor<T: TokenGenerator> {
    generator: T,
    min_length: Option<i64>,
    max_length: Option<i64>,
    use_full_sentences: bool,
    current_length: i64,
    is_at_end: bool,
    sentence_end_tokens: Vec<i64>,
}

pub struct TokenProcessorBuilder {
    min_length: Option<i64>,
    max_length: Option<i64>,
    use_full_sentences: bool,
}

impl TokenProcessorBuilder {
    pub fn new() -> Self {
        Self {
            max_length: None,
            min_length: None,
            use_full_sentences: false,
        }
    }

    pub fn with_min_length(self, min_length: i64) -> Self {
        Self {
            min_length: Some(min_length),
            ..self
        }
    }

    pub fn with_max_length(self, max_length: i64) -> Self {
        Self {
            max_length: Some(max_length),
            ..self
        }
    }

    pub fn with_full_sentences(self) -> Self {
        Self {
            use_full_sentences: true,
            ..self
        }
    }

    pub fn build<'a, M: GeneratorModel>(
        self,
        model: &'a M,
        generator: M::Generator<'a>,
    ) -> TokenProcessor<M::Generator<'a>> {
        let sentence_end_tokens = vec![".", "!", "?", "\u{010a}", ".\"", "!\"", "?\"", "\"."]
            .into_iter()
            .map(|token| model.get_token_id(token))
            .collect();

        TokenProcessor {
            min_length: self.min_length,
            max_length: self.max_length,
            use_full_sentences: self.use_full_sentences,
            generator,
            current_length: 0,
            is_at_end: false,
            sentence_end_tokens,
        }
    }
}

impl<T: TokenGenerator> TokenProcessor<T> {
    fn next_generator_token(&mut self) -> Option<i64> {
        self.generator.next()
    }

    fn next_min_length_token<F: Fn(&mut Self) -> Option<i64>>(&mut self, f: F) -> Option<i64> {
        if let Some(min_length) = self.min_length {
            if self.current_length >= min_length {
                f(self)
            } else {
                match f(self) {
                    Some(token) => Some(token),
                    None => self.next_min_length_token(f),
                }
            }
        } else {
            f(self)
        }
    }

    fn next_max_length_token<F: Fn(&mut Self) -> Option<i64>>(&mut self, f: F) -> Option<i64> {
        if let Some(max_length) = self.max_length {
            if self.current_length > max_length {
                None
            } else {
                f(self)
            }
        } else {
            f(self)
        }
    }

    fn next_full_sentence_token<F: Fn(&mut Self) -> Option<i64>>(&mut self, f: F) -> Option<i64> {
        if self.use_full_sentences {
            if self.is_at_end {
                self.is_at_end = false;
                return None;
            }

            let token = f(self);
            match token {
                None => self.next_full_sentence_token(f),
                Some(token) => {
                    if self.sentence_end_tokens.contains(&token) {
                        self.is_at_end = true;
                    } else {
                        self.is_at_end = false;
                    }
                    Some(token)
                }
            }
        } else {
            f(self)
        }
    }

    pub fn next_token(&mut self) -> Option<i64> {
        let token = self.next_max_length_token(|m| {
            m.next_min_length_token(|m| m.next_full_sentence_token(|m| m.next_generator_token()))
        });
        if token.is_some() {
            self.current_length += 1;
        }
        token
    }
}

impl<T: TokenGenerator> Iterator for TokenProcessor<T> {
    type Item = i64;

    fn next(&mut self) -> Option<Self::Item> {
        self.next_token()
    }
}

impl<T: TokenGenerator> TokenGenerator for TokenProcessor<T> {
    fn reset(&mut self) {
        self.is_at_end = false;
        self.current_length = 0;
    }
}

pub struct BaseTokenDecoder<'a, M: ModelTokens> {
    model_tokens: &'a M,
    stored_tokens: Vec<i64>,
}

pub struct TokenDecoder<'a, T: TokenGenerator, M: ModelTokens> {
    token_generator: T,
    decoder: BaseTokenDecoder<'a, M>,
}

impl<'a, M: ModelTokens> BaseTokenDecoder<'a, M> {
    pub fn new(model_tokens: &'a M) -> Self {
        Self {
            model_tokens,
            stored_tokens: Vec::new(),
        }
    }

    pub fn decode(&mut self, next_token: i64) -> Option<String> {
        self.stored_tokens.push(next_token);
        let text = self.model_tokens.decode_text(self.stored_tokens.clone());
        // continue decoding if we have an invalid codepoint in the output
        if text.contains('\u{fffd}') {
            None
        } else {
            self.stored_tokens.clear();
            Some(text)
        }
    }

    pub fn next_token<T: TokenGenerator + ?Sized>(&mut self, generator: &mut T) -> Option<String> {
        loop {
            match generator.next() {
                None => return None,
                Some(token) => {
                    if let Some(token) = self.decode(token) {
                        return Some(token);
                    }
                }
            }
        }
    }
}

impl<'a, T: TokenGenerator, M: ModelTokens> TokenDecoder<'a, T, M> {
    pub fn new(token_generator: T, model_tokens: &'a M) -> Self {
        Self {
            token_generator,
            decoder: BaseTokenDecoder::new(model_tokens),
        }
    }
}

impl<'a, T: TokenGenerator, M: ModelTokens> Iterator for TokenDecoder<'a, T, M> {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        self.decoder.next_token(&mut self.token_generator)
    }
}
