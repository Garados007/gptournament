use super::{
    GenerationOptions, GeneratorModel, GptTokenGenerator, Model, ModelTokens, TokenGenerator,
};
use crate::error::{GptError, ModelError, ThreadedModelError};
use std::collections::HashMap;
use std::sync::{mpsc, Mutex};
use std::thread::JoinHandle;

pub struct ThreadedModel {
    tx: mpsc::SyncSender<(ModelMessage, mpsc::SyncSender<ModelResult>)>,
    rx: Mutex<mpsc::Receiver<ModelResult>>,
    _handle: JoinHandle<Result<(), ThreadedModelError>>,
    model_loaded: bool,
}

pub struct ThreadedTokenGenerator {
    id: i32,
    tx: mpsc::SyncSender<(ModelMessage, mpsc::SyncSender<ModelResult>)>,
}

#[derive(Debug)]
pub enum ModelMessage {
    NewGenerator(String, GenerationOptions),
    DecodeTokens(Vec<i64>),
    GetTokenID(String),
    DropGenerator(i32),
    NextToken(i32),
}

#[derive(Debug)]
pub enum ModelResult {
    ModelError(GptError),
    ModelLoaded,
    Generator(i32),
    Text(String),
    TokenID(i64),
    Token(Option<i64>),
}

struct InternalGenerator<'a> {
    generator: GptTokenGenerator<'a>,
}

impl Default for ThreadedModel {
    fn default() -> Self {
        Self::new()
    }
}

impl ThreadedModel {
    pub fn new() -> Self {
        log::debug!("Starting model thread");
        let (tx1, rx1) = mpsc::sync_channel(8192);
        let (tx2, rx2) = mpsc::sync_channel(8192);
        let handle = std::thread::spawn(|| model_thread(rx1, tx2));

        Self {
            tx: tx1,
            rx: Mutex::new(rx2),
            _handle: handle,
            model_loaded: false,
        }
    }

    pub fn wait_for_load(&mut self) -> Result<(), ThreadedModelError> {
        if self.model_loaded {
            return Ok(());
        }
        log::debug!("Waiting for model load");
        let msg = {
            let rx = self.rx.lock().unwrap();
            log_receive(rx.recv()?)
        };
        match msg {
            ModelResult::ModelLoaded => {
                log::info!("Model loaded");
                self.model_loaded = true;
                Ok(())
            }
            ModelResult::ModelError(e) => Err(e.into()),
            r => Err(ThreadedModelError::UnexpectedModelResult(r)),
        }
    }

    pub fn get_generator(
        &self,
        text: String,
        generation_options: GenerationOptions,
    ) -> Result<ThreadedTokenGenerator, ThreadedModelError> {
        self.ensure_model()?;
        log::debug!("Requested new generator");
        let msg = log_receive(self.send(log_send(ModelMessage::NewGenerator(
            text,
            generation_options,
        )))?);
        match msg {
            ModelResult::Generator(id) => {
                log::debug!("Created new generator with id {}", id);
                Ok(ThreadedTokenGenerator {
                    id,
                    tx: self.tx.clone(),
                })
            }
            r => Err(ThreadedModelError::UnexpectedModelResult(r)),
        }
    }

    pub fn decode_text(&self, tokens: Vec<i64>) -> Result<String, ThreadedModelError> {
        self.ensure_model()?;
        let msg = log_receive(self.send(log_send(ModelMessage::DecodeTokens(tokens)))?);
        match msg {
            ModelResult::Text(text) => Ok(text),
            r => Err(ThreadedModelError::UnexpectedModelResult(r)),
        }
    }

    pub fn get_token_id(&self, token: String) -> Result<i64, ThreadedModelError> {
        self.ensure_model()?;
        let msg = log_receive(self.send(log_send(ModelMessage::GetTokenID(token)))?);
        match msg {
            ModelResult::TokenID(id) => Ok(id),
            r => Err(ThreadedModelError::UnexpectedModelResult(r)),
        }
    }

    fn ensure_model(&self) -> Result<(), ThreadedModelError> {
        if self.model_loaded {
            Ok(())
        } else {
            Err(ThreadedModelError::ModelNotLoaded)
        }
    }

    fn send(&self, msg: ModelMessage) -> Result<ModelResult, ThreadedModelError> {
        let (tx, rx) = mpsc::sync_channel(1024);
        self.tx.send((msg, tx))?;
        Ok(rx.recv()?)
    }
}

impl ModelTokens for ThreadedModel {
    fn decode_text(&self, tokens: Vec<i64>) -> String {
        self.decode_text(tokens).expect("failed to decode text")
    }

    fn get_token_id(&self, token: &str) -> i64 {
        self.get_token_id(String::from(token))
            .expect("failed to get token id")
    }
}

impl GeneratorModel for ThreadedModel {
    type Generator<'a> = ThreadedTokenGenerator;

    fn create_token_generator(
        &self,
        text: &str,
        generation_options: GenerationOptions,
    ) -> Result<Self::Generator<'_>, ModelError> {
        Ok(self.get_generator(String::from(text), generation_options)?)
    }
}

impl ThreadedTokenGenerator {
    pub fn next_token(&self) -> Result<Option<i64>, ThreadedModelError> {
        let msg = log_receive(self.send(log_send(ModelMessage::NextToken(self.id)))?);
        match msg {
            ModelResult::Token(token) => Ok(token),
            r => Err(ThreadedModelError::UnexpectedModelResult(r)),
        }
    }

    fn send(&self, msg: ModelMessage) -> Result<ModelResult, ThreadedModelError> {
        let (tx, rx) = mpsc::sync_channel(1024);
        self.tx.send((msg, tx))?;
        Ok(rx.recv()?)
    }
}

impl Iterator for ThreadedTokenGenerator {
    type Item = i64;

    fn next(&mut self) -> Option<Self::Item> {
        self.next_token().ok().flatten()
    }
}

impl TokenGenerator for ThreadedTokenGenerator {
    fn reset(&mut self) {}
}

impl Drop for ThreadedTokenGenerator {
    fn drop(&mut self) {
        let _ = self.send(ModelMessage::DropGenerator(self.id));
    }
}

fn model_thread(
    rx: mpsc::Receiver<(ModelMessage, mpsc::SyncSender<ModelResult>)>,
    tx: mpsc::SyncSender<ModelResult>,
) -> Result<(), ThreadedModelError> {
    let model = match Model::new() {
        Ok(model) => model,
        Err(err) => {
            tx.send(log_send(ModelResult::ModelError(err)))?;
            return Ok(());
        }
    };
    tx.send(log_send(ModelResult::ModelLoaded))?;
    drop(tx);
    let model = &model;

    let mut generators = HashMap::new();
    let mut generator_id = 0;

    while let Ok((msg, tx)) = log_receive(rx.recv()) {
        match msg {
            ModelMessage::NewGenerator(text, opts) => {
                let input = model.encode_text(&text);
                let generator = model.get_generator(input, opts);
                generators.insert(generator_id, InternalGenerator { generator });
                tx.send(log_send(ModelResult::Generator(generator_id)))?;
                generator_id += 1;
            }
            ModelMessage::DropGenerator(id) => {
                generators.remove(&id);
            }
            ModelMessage::NextToken(id) => {
                if let Some(generator) = generators.get_mut(&id) {
                    tx.send(log_send(ModelResult::Token(generator.generator.next())))?;
                }
            }
            ModelMessage::DecodeTokens(tokens) => {
                tx.send(log_send(ModelResult::Text(model.decode_text(tokens))))?;
            }
            ModelMessage::GetTokenID(token) => {
                tx.send(log_send(ModelResult::TokenID(model.get_token_id(&token))))?;
            }
        }
    }

    Ok(())
}

fn log_receive<T: std::fmt::Debug>(msg: T) -> T {
    log::trace!("received message: {:?}", msg);
    msg
}

fn log_send<T: std::fmt::Debug>(msg: T) -> T {
    log::trace!("sending message: {:?}", msg);
    msg
}
