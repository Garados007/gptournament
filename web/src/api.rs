mod generator;
mod key;
mod types;

use crate::state::{Games, Generators, StateId};
use generator::{GameGenerator, Generator};
use gpt::game::Game;
use gpt::model::{
    processing::TokenProcessorBuilder, GenerationOptions, ThreadedModel, TokenGenerator,
};
use key::ApiKey;
use rocket::response::{content::Json, Stream};
use rocket::{get, post, Data, State};
use rocket_contrib::json::Json as Json2;
use std::io::Read;
use types::*;

#[get("/api/create?<text>")]
pub fn create(
    text: String,
    _key: ApiKey,
    model: State<ThreadedModel>,
    generators: State<Generators>,
) -> Json<String> {
    let generator = model
        .get_generator(text, GenerationOptions::default())
        .expect("generator");
    let processing = TokenProcessorBuilder::new()
        .with_full_sentences()
        .with_min_length(50)
        .with_max_length(200)
        .build(model.inner(), generator);
    let id = generators.add_state(processing);
    Json(format!("{{ \"id\": \"{}\" }}", id))
}

#[post("/api/create", data = "<data>")]
pub fn create_post(
    data: Data,
    key: ApiKey,
    model: State<ThreadedModel>,
    generators: State<Generators>,
) -> Result<Json<String>, rocket::http::Status> {
    let text = read_body_buffered(data, 8192);
    Ok(create(text, key, model, generators))
}

#[get("/api/generate?<id>")]
pub fn generate<'a>(
    id: u32,
    _key: ApiKey,
    model: State<'a, ThreadedModel>,
    generators: State<'a, Generators>,
) -> Result<Stream<Generator<'a, ThreadedModel>>, rocket::http::Status> {
    match generators.get_inner(id) {
        None => Err(rocket::http::Status::NotFound),
        Some(generator) => {
            {
                let mut g = generator.lock().unwrap();
                g.reset();
            }
            let generator = Generator::new(generator, model.inner());
            Ok(Stream::chunked(generator, 20))
        }
    }
}

#[get("/api/game/create")]
pub fn create_game<'a>(
    _key: ApiKey,
    model: State<'a, ThreadedModel>,
    games: State<'a, Games>,
) -> Json2<IdResponse> {
    let game = match Game::new(model.inner(), GenerationOptions::default()) {
        Ok(game) => game,
        Err(e) => {
            log::error!("Failed to create a game: {}", e);
            return Json2(IdResponse::failure(String::from("failed to create a game")));
        }
    };
    let id = games.add_state(game);
    Json2(IdResponse::success(id))
}

#[get("/api/game/add_input?<id>&<line>")]
pub fn add_input<'a>(
    _key: ApiKey,
    id: StateId,
    line: String,
    model: State<'a, ThreadedModel>,
    games: State<'a, Games>,
) -> Json2<EmptyResponse> {
    let result = games.with_game_mut(id, &model, |game| game.add_input_line(&line));
    match result {
        None => Json2(EmptyResponse::failure(String::from("wrong game id"))),
        Some(Ok(_)) => Json2(EmptyResponse::success()),
        Some(Err(e)) => Json2(EmptyResponse::failure(format!("{:?}", e))),
    }
}

#[post("/api/game/add_input?<id>", data = "<data>")]
pub fn add_input_post<'a>(
    key: ApiKey,
    id: StateId,
    data: Data,
    model: State<'a, ThreadedModel>,
    games: State<'a, Games>,
) -> Json2<EmptyResponse> {
    let text = read_body_buffered(data, 512);
    add_input(key, id, text, model, games)
}

#[get("/api/game/generate?<id>")]
pub fn generate_game<'a>(
    _key: ApiKey,
    id: StateId,
    model: State<'a, ThreadedModel>,
    games: State<'a, Games>,
) -> Result<Stream<GameGenerator<'a>>, rocket::http::Status> {
    let game = if let Some(game) = games.get_inner(id) {
        game
    } else {
        return Err(rocket::http::Status::BadRequest);
    };
    {
        let mut game = game.lock().unwrap();
        game.game_mut(model.inner()).reset_generator();
    }
    let generator = GameGenerator::new(game, model.inner());
    Ok(Stream::chunked(generator, 20))
}

fn read_body_buffered(data: Data, buffer_size: usize) -> String {
    let buf = ringbuf::RingBuffer::new(buffer_size);
    let (mut producer, mut consumer) = buf.split();
    let mut data = data.open();
    let mut temp = [0; 512];
    while let Ok(len) = data.read(&mut temp) {
        if len == 0 {
            break;
        }
        if producer.remaining() < len {
            consumer.discard(len - producer.remaining());
        }
        producer.push_slice(&temp[0..len]);
    }
    let mut text = String::new();
    consumer.access(|head, tail| {
        let mut text_bytes = Vec::with_capacity(consumer.len());
        text_bytes.extend_from_slice(head);
        text_bytes.extend_from_slice(tail);
        text.push_str(&String::from_utf8_lossy(&text_bytes));
    });
    text
}
