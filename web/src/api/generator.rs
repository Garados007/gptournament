use crate::state::{Game, SingleState};
use gpt::model::{processing::BaseTokenDecoder, ModelTokens, ThreadedModel, TokenGenerator};
use std::ops::DerefMut;
use std::sync::{Arc, Mutex};

pub struct Generator<'a, M: ModelTokens> {
    generator: Arc<Mutex<dyn TokenGenerator + Send + Sync>>,
    decoder: BaseTokenDecoder<'a, M>,
    current_string: String,
    eos: bool,
}

impl<'a, M: ModelTokens> Generator<'a, M> {
    pub fn new(generator: Arc<Mutex<dyn TokenGenerator + Send + Sync>>, decoder: &'a M) -> Self {
        Self {
            generator,
            decoder: BaseTokenDecoder::new(decoder),
            eos: false,
            current_string: String::new(),
        }
    }

    fn next_token(&mut self) -> Option<String> {
        let mut generator = self.generator.lock().unwrap();
        self.decoder.next_token(generator.deref_mut())
    }
}

impl<'a, M: ModelTokens> std::io::Read for Generator<'a, M> {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        if self.eos {
            self.eos = false;
            return Ok(0);
        }
        if self.current_string.is_empty() {
            if let Some(token) = self.next_token() {
                self.current_string.push_str(&token);
            } else {
                self.eos = true;
                return Ok(0);
            }
        }
        let mut bytes = self.current_string.as_bytes();
        match bytes.read(buf) {
            Ok(len) => {
                self.current_string = String::from_utf8_lossy(bytes).to_string();
                Ok(len)
            }
            Err(e) => Err(e),
        }
    }
}

pub struct GameGenerator<'a> {
    game_state: Arc<Mutex<SingleState<Game>>>,
    model: &'a ThreadedModel,
    current_string: String,
    eos: bool,
}

impl<'a> GameGenerator<'a> {
    pub fn new(game: Arc<Mutex<SingleState<Game>>>, model: &'a ThreadedModel) -> Self {
        Self {
            game_state: game,
            model,
            eos: false,
            current_string: String::new(),
        }
    }

    fn next_token(&mut self) -> Option<String> {
        let mut game = self.game_state.lock().unwrap();
        let game = game.game_mut(self.model);
        game.next()
    }
}

impl<'a> std::io::Read for GameGenerator<'a> {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        if self.eos {
            self.eos = false;
            return Ok(0);
        }
        if self.current_string.is_empty() {
            if let Some(token) = self.next_token() {
                self.current_string.push_str(&token);
            } else {
                self.eos = true;
                return Ok(0);
            }
        }
        let mut bytes = self.current_string.as_bytes();
        match bytes.read(buf) {
            Ok(len) => {
                self.current_string = String::from_utf8_lossy(bytes).to_string();
                Ok(len)
            }
            Err(e) => Err(e),
        }
    }
}
