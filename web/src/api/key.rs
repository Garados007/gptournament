use crate::config::Config;
use rocket::http::Status;
use rocket::request::{FromRequest, Outcome};
use rocket::{Request, State};

pub struct ApiKey {
    key: String,
}

impl ApiKey {
    fn key_is_valid(config: &Config, key: &str) -> bool {
        config.api.allowed_keys.iter().any(|k| k == key)
    }

    pub fn get_key(&self) -> &str {
        &self.key
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for ApiKey {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> Outcome<Self, Self::Error> {
        let config = request.guard::<State<Config>>()?;
        let keys = request.headers().get("x-api-key").collect::<Vec<_>>();
        match keys.len() {
            1 if Self::key_is_valid(&config, keys[0]) => Outcome::Success(Self {
                key: String::from(keys[0]),
            }),
            _ => Outcome::Failure((Status::BadRequest, ())),
        }
    }
}
