use crate::state::StateId;
use serde::Serialize;

#[derive(Serialize)]
pub enum Status {
    Ok,
    Error,
}

#[derive(Serialize)]
pub struct IdResponse {
    status: Status,
    id: StateId,
    msg: String,
}

impl IdResponse {
    pub fn success(id: StateId) -> Self {
        Self {
            status: Status::Ok,
            msg: String::from(""),
            id,
        }
    }

    pub fn failure(msg: String) -> Self {
        Self {
            status: Status::Error,
            msg,
            id: 0,
        }
    }
}

#[derive(Serialize)]
pub struct EmptyResponse {
    status: Status,
    msg: String,
}

impl EmptyResponse {
    pub fn success() -> Self {
        Self {
            status: Status::Ok,
            msg: String::from(""),
        }
    }

    pub fn failure(msg: String) -> Self {
        Self {
            status: Status::Error,
            msg,
        }
    }
}
