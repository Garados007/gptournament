use serde::Deserialize;
use std::path::Path;

#[derive(Debug, Deserialize)]
pub struct Config {
    pub api: Api,
    pub secret_key: String,
    pub db_path: String,
    pub admin_password: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct Api {
    pub allowed_keys: Vec<String>,
}

pub fn load_config(path: &Path) -> Result<Config, anyhow::Error> {
    let text = if path.exists() {
        std::fs::read_to_string(path)?
    } else {
        log::warn!("No config file found, using empty config");
        String::from("")
    };
    Ok(toml::from_str(&text)?)
}
