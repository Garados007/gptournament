pub mod model;
mod schema;

use crate::config::Config;
use diesel::prelude::*;
use libreauth::key::KeyBuilder;
use libreauth::pass::{HashBuilder, Hasher};
use rocket::fairing::{Fairing, Info, Kind};
use rocket::Rocket;
use rocket_contrib::database;
use thiserror::Error;

embed_migrations!();

#[database("sqlite_db")]
pub struct DbConn(diesel::SqliteConnection);

#[derive(Error, Debug)]
pub enum DBError {
    #[error("Error from DB driver: {0}")]
    DieselError(#[from] diesel::result::Error),
    #[error("No results found")]
    NoResults,
}

impl DbConn {
    pub fn get_user(&self, name: &str) -> Result<model::User, DBError> {
        use schema::users;

        let user = users::table
            .filter(users::name.eq(name))
            .load::<model::User>(&self.0)?;
        match user.len() {
            1 => Ok(user[0].clone()),
            _ => Err(DBError::NoResults),
        }
    }

    pub fn add_user(&self, name: &str, password: &str, is_admin: bool) -> Result<(), DBError> {
        use schema::users;

        let new_user = model::NewUser {
            name: name.into(),
            password: password.into(),
            is_admin,
        };

        diesel::insert_into(users::table)
            .values(&new_user)
            .execute(&self.0)?;
        Ok(())
    }

    pub fn update_user_password(
        &self,
        user: &model::User,
        new_password: &str,
    ) -> Result<(), DBError> {
        use schema::users;

        diesel::update(user)
            .set(users::password.eq(new_password))
            .execute(&self.0)?;
        Ok(())
    }
}

pub struct DbSetup {}

impl Default for DbSetup {
    fn default() -> Self {
        Self::new()
    }
}

impl DbSetup {
    pub fn new() -> Self {
        Self {}
    }
}

impl Fairing for DbSetup {
    fn info(&self) -> Info {
        Info {
            name: "db setup",
            kind: Kind::Attach,
        }
    }

    fn on_attach(&self, rocket: Rocket) -> Result<Rocket, Rocket> {
        let config = rocket.state::<Config>().expect("config");
        let db: DbConn = DbConn::get_one(&rocket).expect("config");

        log::info!("Running db migrations");
        if let Err(e) = embedded_migrations::run(&db.0) {
            log::error!("Error running migrations: {}", e);
            return Err(rocket);
        }

        let user = db.get_user("admin");
        if let Ok(user) = user {
            match (
                HashBuilder::from_phc(&user.password),
                &config.admin_password,
            ) {
                (Ok(hasher), Some(password)) if !hasher.is_valid(&password) => {
                    log::info!("Updating admin password");
                    match crate::user::build_hasher().hash(&password) {
                        Ok(hash) => {
                            if let Err(e) = db.update_user_password(&user, &hash) {
                                log::error!("Failed to update admin password: {}", e);
                                return Err(rocket);
                            }
                        }
                        Err(e) => {
                            log::error!("Failed to hash admin password: {:?}", e);
                            return Err(rocket);
                        }
                    }
                }
                (Err(e), _) => {
                    log::error!("Failed to build hasher: {:?}", e);
                    return Err(rocket);
                }
                _ => {}
            }
        } else {
            log::info!("No admin user found, creating new admin user");
            let admin_password = match &config.admin_password {
                Some(password) => password.clone(),
                None => {
                    let pass = KeyBuilder::new().size(8).as_hex();
                    log::info!("Generated admin password: {:?}", pass);
                    pass
                }
            };
            if let Err(e) = db.add_user("admin", &admin_password, true) {
                log::error!("Failed to add admin user: {}", e);
                return Err(rocket);
            }
        }

        Ok(rocket)
    }
}
