use super::schema::users;
use diesel::{Identifiable, Insertable, Queryable};

#[derive(Queryable, Identifiable, Clone, Debug)]
pub struct User {
    pub id: i32,
    pub name: String,
    pub password: String,
    pub is_admin: bool,
}

#[derive(Insertable, Clone, Debug)]
#[table_name = "users"]
pub struct NewUser {
    pub name: String,
    pub password: String,
    pub is_admin: bool,
}
