table! {
    users {
        id -> Integer,
        name -> VarChar,
        password -> VarChar,
        is_admin -> Bool,
    }
}
