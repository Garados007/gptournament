#![feature(proc_macro_hygiene, decl_macro, never_type)]

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

mod api;
mod config;
mod db;
mod resources;
mod state;
mod user;

use clap::{App, Arg};
use gpt::model::ThreadedModel;
use rocket::config::{Config, Environment};
use rocket::{get, routes};
use std::path::{Path, PathBuf};
use version::version;

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

#[tokio::main]
async fn main() {
    let crate_setup = bin_common::CrateSetupBuilder::new()
        .with_app_name("gptournament-web")
        .build()
        .expect("CrateSetup");
    let matches = App::new(crate_setup.application_name())
        .version(version!())
        .arg(
            Arg::with_name("v")
                .short("v")
                .long("verbose")
                .multiple(true)
                .help("Increase verbosity. May be specified multiple times."),
        )
        .arg(
            Arg::with_name("model")
                .long("model")
                .takes_value(true)
                .value_name("MODEL PATH")
                .help("Path to model."),
        )
        .arg(
            Arg::with_name("config")
                .long("config")
                .short("c")
                .takes_value(true)
                .value_name("CONFIG_PATH")
                .help("Config file path"),
        )
        .get_matches();
    let verbosity = matches.occurrences_of("v") as u8;
    crate_setup
        .logging_setup()
        .with_log_panics(true)
        .with_verbosity(verbosity)
        .build()
        .expect("logging");

    let config_path = match matches.value_of("config") {
        Some(path) => Ok(PathBuf::from(path)),
        None => std::env::current_dir().map(|p| p.join("config.toml")),
    };
    let config = match config_path {
        Ok(config_path) => match config::load_config(&config_path) {
            Ok(config) => config,
            Err(e) => {
                log::error!("Error loading config: {}", e);
                return;
            }
        },
        Err(e) => {
            log::error!("Error loading config: {}", e);
            return;
        }
    };

    log::debug!("Loaded configuration: {:?}", config);

    log::info!("Starting GPTournament web v{}", version!());

    if let Some(path) = matches.value_of("model") {
        let path = Path::new(path);
        if !path.exists() {
            if let Err(e) = std::fs::create_dir_all(path) {
                log::error!("Failed to create path {:?} for model: {}", path, e);
                return;
            } else {
                log::info!("Created model path {:?}", path);
            }
        }
        log::info!("Model path: {:?}", path);
        gpt::set_model_path(path);
    } else {
        gpt::set_model_path_to_cwd();
    }
    log::debug!("Loading model");
    let mut model = ThreadedModel::new();
    model.wait_for_load().expect("model");

    let rocket_config = Config::build(Environment::active().expect("env"))
        .secret_key(&config.secret_key)
        .extra("databases", {
            let mut map = toml::value::Table::new();
            map.insert("sqlite_db".into(), {
                let mut map = toml::value::Table::new();
                map.insert("url".into(), config.db_path.clone().into());
                map.into()
            });
            map
        })
        .finalize();
    let rocket_config = match rocket_config {
        Ok(config) => config,
        Err(e) => {
            log::error!("Error building config: {}", e);
            return;
        }
    };

    rocket::custom(rocket_config)
        .manage(config)
        .manage(model)
        .manage(state::Generators::new())
        .manage(state::Games::new())
        .attach(resources::StateCleanup::new())
        .attach(db::DbConn::fairing())
        .attach(db::DbSetup::new())
        .mount(
            "/",
            routes![
                index,
                api::generate,
                api::create,
                api::create_post,
                api::create_game,
                api::add_input,
                api::add_input_post,
                api::generate_game
            ],
        )
        .launch();
}
