use crate::state::{Game, ModelState, ModelStateImpl};
use gpt::model::{processing::TokenProcessor, ThreadedTokenGenerator};
use rocket::fairing::{Fairing, Info, Kind};
use rocket::Rocket;
use std::sync::Weak;
use std::thread;
use std::time::Duration;

pub struct StateCleanup {}

impl Default for StateCleanup {
    fn default() -> Self {
        Self::new()
    }
}

impl StateCleanup {
    pub fn new() -> Self {
        Self {}
    }

    fn state_cleanup_thread<T>(states: Weak<ModelStateImpl<T>>) {
        log::info!("Starting cleanup thread");
        loop {
            thread::sleep(Duration::from_secs(60));
            if let Some(states) = states.upgrade() {
                log::debug!("Running cleanup");
                states.cleanup();
            } else {
                break;
            }
        }
    }

    fn start_thread<T: 'static + Send>(rocket: &Rocket) {
        let generators = rocket.state::<ModelState<T>>();
        if let Some(generators) = generators {
            let weak = generators.get_weak();
            std::thread::spawn(|| {
                Self::state_cleanup_thread(weak);
            });
        } else {
            log::error!("Failed to get state!");
        }
    }
}

impl Fairing for StateCleanup {
    fn info(&self) -> Info {
        Info {
            name: "state cleanup",
            kind: Kind::Launch,
        }
    }

    fn on_launch(&self, rocket: &Rocket) {
        Self::start_thread::<TokenProcessor<ThreadedTokenGenerator>>(rocket);
        Self::start_thread::<Game>(rocket);
    }
}
