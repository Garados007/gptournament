use gpt::game::Game as GptGame;
use gpt::model::{
    processing::TokenProcessor, ThreadedModel, ThreadedTokenGenerator, TokenGenerator,
};
use rocket::State;
use std::collections::HashMap;
use std::ops::{Deref, DerefMut};
use std::sync::atomic::{AtomicU32, Ordering};
use std::sync::{Arc, Mutex, RwLock, Weak};
use std::time::{Duration, Instant};

pub type Generators = ModelState<TokenProcessor<ThreadedTokenGenerator>>;
pub type Games = ModelState<Game>;

pub type StateId = u32;

pub struct ModelState<T> {
    inner: Arc<ModelStateImpl<T>>,
}

impl<T> Default for ModelState<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> ModelState<T> {
    pub fn new() -> Self {
        Self {
            inner: Arc::new(ModelStateImpl::new()),
        }
    }

    pub fn get_weak(&self) -> Weak<ModelStateImpl<T>> {
        Arc::downgrade(&self.inner)
    }
}

impl<T> Deref for ModelState<T> {
    type Target = ModelStateImpl<T>;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

pub struct ModelStateImpl<T> {
    states: RwLock<HashMap<StateId, Arc<Mutex<SingleState<T>>>>>,
    current_id: AtomicU32,
}

impl<T> Default for ModelStateImpl<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> ModelStateImpl<T> {
    pub fn new() -> Self {
        Self {
            states: RwLock::new(HashMap::new()),
            current_id: AtomicU32::new(0),
        }
    }

    pub fn get_inner(&self, id: StateId) -> Option<Arc<Mutex<SingleState<T>>>> {
        let states = self.states.read().unwrap();
        if let Some(arc) = states.get(&id) {
            let arc = Arc::clone(arc);
            {
                let mut lock = arc.lock().unwrap();
                lock.last_use = Instant::now();
            }
            Some(arc)
        } else {
            None
        }
    }

    pub fn add_state<U: Into<T>>(&self, state: U) -> StateId {
        let mut states = self.states.write().unwrap();
        let id = self.current_id.fetch_add(1, Ordering::SeqCst);
        states.insert(
            id,
            Arc::new(Mutex::new(SingleState {
                inner: state.into(),
                last_use: Instant::now(),
            })),
        );
        id
    }

    pub fn cleanup(&self) {
        let mut cleanup = Vec::new();
        {
            let states = self.states.read().unwrap();
            for (id, state) in states.iter() {
                cleanup.push((*id, Arc::clone(state)));
            }
        }
        let cleanup = cleanup
            .into_iter()
            .filter(|(_, state)| {
                let state = state.lock().unwrap();
                Instant::now().duration_since(state.last_use) > Duration::from_secs(60 * 10)
            })
            .map(|(id, _)| id)
            .collect::<Vec<_>>();
        if !cleanup.is_empty() {
            log::info!("Cleaning up {} states", cleanup.len());
            let mut states = self.states.write().unwrap();
            for id in cleanup {
                states.remove(&id);
                log::debug!(
                    "Cleaned up state {} with id {}",
                    std::any::type_name::<T>(),
                    id
                );
            }
        }
    }
}

impl ModelStateImpl<Game> {
    pub fn with_game<'a, F: FnOnce(&GptGame<'a>) -> R, R>(
        &self,
        id: StateId,
        model: &State<'a, ThreadedModel>,
        f: F,
    ) -> Option<R> {
        if let Some(inner) = self.get_inner(id) {
            let inner = inner.lock().unwrap();
            Some(f(inner.game(model.inner())))
        } else {
            None
        }
    }
    pub fn with_game_mut<'a, F: FnOnce(&mut GptGame<'a>) -> R, R>(
        &self,
        id: StateId,
        model: &State<'a, ThreadedModel>,
        f: F,
    ) -> Option<R> {
        if let Some(inner) = self.get_inner(id) {
            let mut inner = inner.lock().unwrap();
            Some(f(inner.game_mut(model.inner())))
        } else {
            None
        }
    }
}

pub struct SingleState<T> {
    inner: T,
    last_use: Instant,
}

impl<T> Deref for SingleState<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T> DerefMut for SingleState<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

impl Iterator for SingleState<TokenProcessor<ThreadedTokenGenerator>> {
    type Item = i64;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next()
    }
}

impl TokenGenerator for SingleState<TokenProcessor<ThreadedTokenGenerator>> {
    fn reset(&mut self) {
        self.inner.reset()
    }
}

// FIXME: the following things are *incredibly* unsafe and hacky
//        the lifetime bounds on Game::game() are required to make sure GptGame doesn't live longer
//        than the corresponding model
pub struct Game {
    inner: Box<GptGame<'static>>,
}

impl Game {
    pub fn new<'a>(game: GptGame<'a>) -> Self {
        let game = unsafe { std::mem::transmute::<GptGame<'a>, GptGame<'static>>(game) };
        Self {
            inner: Box::new(game),
        }
    }

    pub fn game<'a>(&self, model: &'a ThreadedModel) -> &GptGame<'a> {
        if std::ptr::eq(
            self.inner.model() as *const ThreadedModel,
            model as *const ThreadedModel,
        ) {
            unsafe { std::mem::transmute::<&GptGame<'static>, &GptGame<'a>>(self.inner.as_ref()) }
        } else {
            panic!("Model pointers not equal, can't guarantee lifetime!");
        }
    }

    pub fn game_mut<'a>(&mut self, model: &'a ThreadedModel) -> &mut GptGame<'a> {
        if std::ptr::eq(
            self.inner.model() as *const ThreadedModel,
            model as *const ThreadedModel,
        ) {
            unsafe {
                std::mem::transmute::<&mut GptGame<'static>, &mut GptGame<'a>>(self.inner.as_mut())
            }
        } else {
            panic!("Model pointers not equal, can't guarantee lifetime!");
        }
    }
}

impl<'a> From<GptGame<'a>> for Game {
    fn from(g: GptGame) -> Self {
        Self::new(g)
    }
}
