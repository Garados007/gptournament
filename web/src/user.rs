use crate::db::DbConn;
use libreauth::pass::{HashBuilder, Hasher};
use rocket::http::{Cookie, Cookies};
use rocket::request::{Form, FromForm, FromRequest, Outcome};
use rocket::{get, post, Request};
use rocket_contrib::json::Json;
use serde::{Deserialize, Serialize};

#[derive(FromForm)]
pub struct UserLogin {
    username: String,
    password: String,
}

#[derive(Serialize)]
enum Status {
    Ok,
    Error(String),
}

#[derive(Serialize)]
pub struct StatusResponse {
    status: Status,
}

#[derive(Serialize)]
pub struct TokenResponse {
    status: Status,
    token: String,
}

pub fn build_hasher() -> Hasher {
    HashBuilder::new().finalize().unwrap()
}

fn validate_user(login: UserLogin, db: &DbConn) -> Option<UserData> {
    if let Ok(user) = db.get_user(&login.username) {
        let hasher = HashBuilder::from_phc(&user.password).ok()?;
        if hasher.is_valid(&login.password) {
            Some(UserData {
                username: login.username,
            })
        } else {
            None
        }
    } else {
        None
    }
}

#[post("/login?result=json", data = "<login>")]
pub fn login_json(
    login: Form<UserLogin>,
    mut cookies: Cookies,
    db: DbConn,
) -> Json<StatusResponse> {
    let login = login.into_inner();
    if let Some(user_data) = validate_user(login, &db) {
        cookies.add_private(Cookie::new(
            "user",
            serde_json::to_string(&user_data).unwrap(),
        ));
        Json(StatusResponse { status: Status::Ok })
    } else {
        Json(StatusResponse {
            status: Status::Error(String::from("invalid username or password")),
        })
    }
}

#[get("/add_user?result=json")]
pub fn add_user_json(_admin: Admin) -> Json<TokenResponse> {
    // TODO: actually generate and store token
    let token = String::from("1234567890");
    Json(TokenResponse {
        status: Status::Ok,
        token,
    })
}

#[post("/add_user?result=json&<token>", data = "<login>")]
pub fn add_user_json_token(token: String, login: Form<UserLogin>) -> Json<StatusResponse> {
    // TODO: verify token and create user
    Json(StatusResponse { status: Status::Ok })
}

#[derive(Serialize, Deserialize)]
struct UserData {
    username: String,
}

pub struct User {
    username: String,
    is_admin: bool,
}

pub struct Admin<'a>(&'a User);

impl User {
    fn from_model(user: crate::db::model::User) -> Self {
        Self {
            username: user.name,
            is_admin: user.is_admin,
        }
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for &'a User {
    type Error = !;

    fn from_request(request: &'a Request<'r>) -> Outcome<Self, Self::Error> {
        let user = request.local_cache(|| {
            let mut cookies = request.cookies();
            let user = cookies
                .get_private("user")
                .map(|cookie| serde_json::from_str::<UserData>(cookie.value()));
            let user = match user {
                Some(Ok(user)) => user,
                _ => return None,
            };

            let db = match request.guard::<DbConn>() {
                Outcome::Success(s) => s,
                Outcome::Failure(_f) => return None,
                Outcome::Forward(_f) => return None,
            };
            let user = match db.get_user(&user.username) {
                Ok(user) => user,
                Err(e) => {
                    log::error!("Error querying user from db: {}", e);
                    return None;
                }
            };
            Some(User::from_model(user))
        });

        match user {
            Some(user) => Outcome::Success(user),
            None => Outcome::Forward(()),
        }
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for Admin<'a> {
    type Error = !;

    fn from_request(request: &'a Request<'r>) -> Outcome<Self, Self::Error> {
        let user = request.guard::<&'a User>()?;
        if user.is_admin {
            Outcome::Success(Admin(user))
        } else {
            Outcome::Forward(())
        }
    }
}
